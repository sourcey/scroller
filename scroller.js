/*
    Scroller.js
    
    Implements a JavaScript powered CSS customisable 
    scrollbar for scrolling HTML elements.

    @requires jquery.js
    @requires jquery.event.drag.js
    @requires jquery.mousewheel.js
    @requires jquery.mousehold.js (see options)
 
    Copyright (c)2010 Sourcey
    http://sourcey.com
    Distributed under The MIT License.
*/

 
(function ($) {

    $.scroller = $.scroller || {};

    // Default options
    $.scroller.options = {
        size: 'auto',        // size of the scrollbar - auto or a fixed number
        autohide: true,     // auto hides the scrollbar on mouse out
        arrows: false,      // show scroll arrows (requires jquery.mousehold.js)
        onscroll: null      // scroll event callback
    };    
    
    // API methods
    $.scroller.methods = {
        create: function(options) {
            options = $.extend({}, $.scroller.options, options);
            return $(this).each(function () {
                if ($(this).data('scroller')) {
                    $(this).scroller('update');
                } else {
                    $(this).data('scroller', new Scrollbar(this, options));
                }
            })
        },
        
        update: function() {
            return this.each(function () {            
                if ($(this).data('scroller'))
                    $(this).data('scroller').update();
            });
        },
        
        destroy: function() {
            return this.each(function () {
                $(this).data('scroller').destroy();
                $(this).data('scroller', null);
            });
        }
    }

    //
    // JQuery cunstructor
    $.fn.scroller = function (method) {  
        if ($.scroller.methods[method])
            return $.scroller.methods[method].apply(this, Array.prototype.slice.call(arguments, 1));            
        else if (typeof method === "object" || !method)
            return $.scroller.methods.create.apply(this, arguments);            
        else
            $.error("Method '" + method + "' does not exist for $.fn.scroller");
    };

    //
    // The scrollbar class
    function Scrollbar(el, options) {                
        var root = $(el),
            scrollWrap,
            scrollView,
            scrollContent;

        // Build HTML if needed
        if (!root.hasClass("scroller") &&
          root.find("> .scroll-wrap").length == 0) {
          root.addClass("scroller");
          root.addClass(options.arrows ? "has-arrows" : "no-arrows");

          scrollWrap = $('<div class="scroll-wrap"></div>');
          scrollView = $('<div class="scroll-view"></div>');
          scrollContent = $('<div class="scroll-content"></div>');
        
          root.wrapInner(scrollContent).wrapInner(scrollView).wrapInner(scrollWrap);
        }

        scrollWrap = root.find(".scroll-wrap");
        scrollView = root.find(".scroll-view");

        var vScroller = $('<div class="scroll-bar v-scroll"></div>'),
          vThumb = $('<div class="scroll-thumb"></div>'),
          vTrack = $('<div class="scroll-track"></div>');

        vTrack.append(vThumb);
        vScroller.append(vTrack);

        var hScroller = $('<div class="scroll-bar h-scroll"></div>'),
          hThumb = $('<div class="scroll-thumb"></div>'),
          hTrack = $('<div class="scroll-track"></div>');

        hTrack.append(hThumb);
        hScroller.append(hTrack);

        if (options.arrows) {
            var scrollUp = $('<div class="scroll-btn scroll-up">▲</div>'),
              scrollDown = $('<div class="scroll-btn scroll-down">▼</div>');
            vScroller.append(scrollUp).append(scrollDown);
            
            var scrollLeft = $('<div class="scroll-btn scroll-left">◄</div>'),
              scrollRight = $('<div class="scroll-btn scroll-right">►</div>');
            hScroller.append(scrollLeft).append(scrollRight);
        }

        var scrollBreak = $('<div class="scroll-break"></div>');
        root.append(vScroller).append(scrollBreak).append(hScroller);
           
        // Set instance variables 
        this.options = options;
        this.root = root;
        this.content = scrollWrap;
        this.wrapper = scrollView;

        //
        // Bind Events
        this.bind = function () {
            var self = this;
            
            if (options.size == 'auto') {
              $(window).bind(self.update);
              //$(window).bind(jQuery.event.special.resizeend ? 'resizeend' : 'resize', function() {
              //   self.update();
              //});
            }

            // Allow the directional buttons to scroll the content.
            // Requires jquery.mousehold.js
            if (options.arrows) {
                scrollUp.mousehold(100, function () {
                    scrollWrap.scrollTop(scrollWrap.scrollTop() - 40);
                    self.update();
                });

                scrollDown.mousehold(100, function () {
                    scrollWrap.scrollTop(scrollWrap.scrollTop() + 40);
                    self.update();
                });

                scrollLeft.mousehold(100, function () {
                    scrollWrap.scrollLeft(scrollWrap.scrollLeft() - 40);
                    self.update();
                });

                scrollRight.mousehold(100, function () {
                    scrollWrap.scrollLeft(scrollWrap.scrollLeft() + 40);
                    self.update();
                });
            }

            // Allow the thumb block to be dragged
            vThumb.drag("start", function (e, dd) {
                dd.origTop = $(this).position().top;
            }).drag(function (e, dd) {
                var maxTop = $(this).parent().innerHeight() - $(this).outerHeight();
                var newTop = Math.max(0, Math.min(maxTop, dd.origTop + dd.deltaY));

                $(this).css('top', newTop);
                scrollWrap.scrollTop((newTop / maxTop) * (scrollView.outerHeight() - scrollWrap.innerHeight()));
            });

            hThumb.drag("start", function (e, dd) {
                dd.origLeft = $(this).position().left;
            }).drag(function (e, dd) {
                var maxLeft = $(this).parent().innerWidth() - $(this).outerWidth();
                var newLeft = Math.max(0, Math.min(maxLeft, dd.origLeft + dd.deltaX));

                $(this).css('left', newLeft);
                scrollWrap.scrollLeft((newLeft / maxLeft) * (scrollView.outerWidth() - scrollWrap.innerWidth()));
            });

            // Allow the mouse wheel to scroll the content
            root.mousewheel(function (e, delta) {
                scrollWrap.scrollTop(scrollWrap.scrollTop() - (delta * 30));
                self.update();
                return false;
            });

            // Allow pressing the scroll track to move the scroll bar
            vTrack.mousedown(function (e) {
                var maxTop = $(this).innerHeight() - vThumb.outerHeight();
                var newTop = Math.max(0, Math.min(maxTop, e.pageY - $(this).offset().top - (vThumb.outerHeight() / 2.0)));

                vThumb.css('top', newTop);
                scrollWrap.scrollTop((newTop / maxTop) * (scrollView.outerHeight() - scrollWrap.innerHeight()));
            });

            hTrack.mousedown(function(e) {
                var maxLeft = $(this).innerWidth() - hThumb.outerWidth();
                var newLeft = Math.max(0, Math.min(maxLeft, e.pageX - $(this).offset().left - (hThumb.outerWidth() / 2.0)));

                vThumb.css('left', newLeft);
                scrollWrap.scrollLeft((newLeft / maxLeft) * (scrollView.outerWidth() - scrollWrap.innerWidth()));
            });

            scrollWrap.scroll(function(e) {
                if (options.onscroll)
                    options.onscroll(e)
                self.update();
            });
                        
            // TODO: Scroll height depends on track visibility, 
            // may break un update if hidden resulting in 0 height.
            if (options.autohide) {
                vScroller.hide();
                hScroller.hide();
                root.hover(
                    function() {
                        //console.log('Scroller: Over')
                        $.data(this, 'hover', true);
                        self.updateVisibility();
                    },
                    function() {
                        //console.log('Scroller: Out')
                        $.data(this, 'hover', false);
                        self.updateVisibility();
                    }
                    ).data('hover', false);
            }
        }

        //
        // Destroy the scroller
        this.destroy = function () {
            var contents =  scrollView.contents().detach();                
            root.html('').append(contents)
            root.removeClass("scroller has-arrows no-arrows");
            $(window).unbind(self.update);
        }

        //
        // Update the scroll position
        this.update = function () {

            // Determine the size and position of the scroller based on the scroll 
            // position and view size/scroll size
            var amountHeight = scrollWrap.innerHeight() / scrollView.outerHeight();
            if (amountHeight >= 1) {
                // If we do not need some scroller, then we simply set the class of the
                // scroller element and let css remove them
                root.addClass("no-scroll-v");
            } 
            else {
                root.removeClass("no-scroll-v");

                var vScrollHeight = amountHeight * vTrack.innerHeight();
                var availableHeight = scrollView.outerHeight() - scrollWrap.innerHeight();
                var amountTop = scrollWrap.scrollTop() / availableHeight;
                var vScrollTop = amountTop * (vTrack.innerHeight() - vScrollHeight);

                vThumb.css({
                    top: vScrollTop,
                    height: vScrollHeight
                });
            }

            // Same for the horizontal bar
            var amountWidth = scrollWrap.innerWidth() / scrollView.outerWidth();
            if (amountWidth >= 1) {
                // If we do not need some scroller, then we simply set the class of the 
                // scroller element and let css remove them
                root.addClass("no-scroll-h");
            }
            else {
                root.removeClass("no-scroll-h");

                var hScrollWidth = amountWidth * hTrack.innerWidth();
                var availableWidth = scrollView.outerWidth() - scrollWrap.innerWidth();
                var amountLeft = scrollWrap.scrollLeft() / availableWidth;
                var hScrollLeft = amountLeft * (hTrack.innerWidth() - hScrollWidth);

                hThumb.css({
                    left: hScrollLeft,
                    width: hScrollWidth
                });
            }
            
            //console.log('Scroller: update:', amountHeight, amountWidth)
        };
                
        //
        // Update scroll bar visibility depending on scroll and hover state
        this.updateVisibility = function () {
        
            // If not visible and hovered show the scroller
            if (root.data('hover')) {
                if (!root.hasClass("no-scroll-v")) //vScroller.is(':hidden') && 
                    vScroller.stop(true, true).fadeIn();
                if (!root.hasClass("no-scroll-h")) //hScroller.is(':hidden') && 
                    hScroller.stop(true, true).fadeIn();            
            }
            
            // If visible and not hovered or scrolling hide the scroller
            else { //if (!root.data('hover') && !root.data('scrolling')) {
                if (vScroller.is(':visible') && !root.hasClass("no-scroll-v"))
                    vScroller.stop(true, true).delay(600).fadeOut();
                if (hScroller.is(':visible') && !root.hasClass("no-scroll-h"))
                    hScroller.stop(true, true).delay(600).fadeOut();
            }
        }

        // Initialize
        this.update();
        this.bind();
    };
})(jQuery);